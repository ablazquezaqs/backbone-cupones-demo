

var faker = require('faker')

function generateData() {
    var customers = [];
    var vouchers = [];
    var voucherMovements = [];
    faker.locale = "es";

    customers = createCustomers();
    vouchers = createVouchers(customers);
    voucherMovements = createVoucherMovements(vouchers);

    return {
        "customers": customers,
        "vouchers": vouchers,
        "voucherMovements": voucherMovements
    }
}

function createCustomers() {
    var customers = [];

    for (var id = 0; id < 50; id++) {
        var firstName = faker.name.firstName();
        var lastName = faker.name.lastName();
        var email = faker.internet.email();
        var phoneNumber = faker.phone.phoneNumberFormat();
        var address = faker.address.streetAddress();
        var options = {
            min: 10000000,
            max: 99999999
        };
        var dni = faker.random.number(options) + faker.random.arrayElement().toUpperCase();
        customers.push({
            "id": id,
            "name": firstName,
            "surname": lastName,
            "email": email,
            "phone": phoneNumber,
            "address": address,
            "dni": dni
        })
    }

    return customers;
}

function createVouchers(customers) {
    var vouchers = [];
    var id = 0;
    for (var i = 0; i < customers.length; i++) {

        var customer = customers[i];
        var nVouchers = faker.random.number({ min: 0, max: 6 });
        for (var j = 0; j < nVouchers; j++) {

            var voucherName = faker.name.title();
            var customerId = customer.id;
            var customerName = customer.name + ' ' + customer.surname;
            var options = {
                min: 10000000000,
                max: 99999999999
            };
            var barcode = faker.random.number(options);
            // var creationDate = moment(faker.date.past()).format('L');
            // var expirationDate = moment(faker.date.future()).format('L');
            var creationDate = faker.date.past().toJSON();
            var expirationDate = faker.date.future().toJSON();
            var initialAmount = faker.random.number({ max: 100 });
            var currentAmount = faker.random.number({ max: 75 });
            var active = faker.random.boolean();

            vouchers.push({
                "id": id++,
                "voucherName": voucherName,
                "customerId": customerId,
                "customerName": customerName,
                "barcode": barcode,
                "creationDate": creationDate,
                "expirationDate": expirationDate,
                "initialAmount": initialAmount,
                "currentAmount": currentAmount,
                "active": active
            });
        };
    };
    return vouchers;
}

function createVoucherMovements(vouchers) {
    var voucherMovements = [];
    var id = 0;
    for (var i = 0; i < vouchers.length; i++) {
        var nVoucherMovements = faker.random.number({ min: 0, max: 6 });
        var voucher = vouchers[i];
        for (var j = 0; j < nVoucherMovements; j++) {

            var movementName = 'Movimiento ' + j;
            var voucherId = voucher.id;
            var storeName = faker.company.companyName();
            var userName = faker.name.firstName();
            var terminalName = faker.name.firstName();
            var customerName = voucher.customerName;
            var movementDate = faker.date.past().toJSON();
            var creationDate = faker.date.past().toJSON();
            var movementType = faker.finance.transactionType();
            var movementQuantity = faker.random.number({ min: 1, max: 4 });
            var movementAmount = faker.random.number({ min: -18, max: -1 });
            var comments = faker.lorem.paragraph(2);
            voucherMovements.push({
                "id": id++,
                "movementName": movementName,
                "voucherId": voucherId,
                "storeName": storeName,
                "userName": userName,
                "terminalName": terminalName,
                "customerName": customerName,
                "movementDate": movementDate,
                "creationDate": creationDate,
                "movementType": movementType,
                "movementQuantity": movementQuantity,
                "movementAmount": movementAmount,
                "comments": comments
            });
        };
    }
    return voucherMovements;
}

// json-server requires that you export
// a function which generates the data set
module.exports = generateData;