# Demo de Backbone.js itilizando la app de cupones.

## Introducción
La aplicación no esta finalizada ni tiene toda la funcionalñidad implementada. Sirve como demo y prueba de concepto para:
* El uso de Backbone.js
* La construccion de buscadores paginados
* La renderizacion y validacion de formularios

## Tecnologías usadas

### Backbone.js
Framework o librería para desarrollo de aplicaciones Javascript con tecnologías web. Se añaden principales enlaces de documentación e información:  
* [Documentacion oficial de Backbone.js](http://backbonejs.org/)
* [EBook de Backbone.js con ejemplos](https://addyosmani.com/backbone-fundamentals/)
### Backbone paginator
Plugin de backbone que provee de colecciones paginables. Facilita la obtencion de datos de las apis que soportan paginación. Se enlace a la documentación del plugin:
* [Documentacion en Github de Backbone Paginator](https://github.com/backbone-paginator/backbone.paginator)
### Backbone forms
Plugin de backbone que facilita la renderizacion de formularios, su validacion y la obtencion del resultado.
* [Documentacion en Github de Backbone Forms](https://github.com/powmedia/backbone-forms)
### JSON server
Se utiliza un servidor de JSON implementado en node.js. Sirve para hacer prototipos de apis REST basadas en ficheros muy rapidas de implementar y con propositos de testing o desarrollo.
* [mocking JSON server](https://coligo.io/create-mock-rest-api-with-json-server/)
### Node y npm
Se usa para gestionar las dependecias web de la app. Dentro de el directorio de la app: ./vouchers ejecutar
```
npm install
```