// Enable debug
$.i18n.debug = true;

// $(document).ready(function () {
$.i18n().load({
	'en': '../common/lang/i18n/demo-en.json',
	'es': '../common/lang/i18n/demo-es.json'
}).done(function () {
	$.i18n().locale = navigator.languages[0];
	$('body').i18n();

	$('.language').change(function (e) {
		e.preventDefault();
		if ($(this).find(':selected') != undefined) {
			$.i18n().locale = $(this).find(':selected').attr('value');
			$('.container').i18n();
		}
	})
}
);
// });
