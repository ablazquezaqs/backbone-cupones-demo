console.log('Init main.js');

var app = app || {};
app.events = app.events || {};

moment.locale('es');
Handlebars.registerHelper('formatDate', function (dateString) {
    var date = new Date(dateString);
    var formatedDate = moment(date).format('L');
    return formatedDate;
});

Handlebars.registerHelper('renderActive', function (isActive) {
    var valueString;
    if (isActive) {
        valueString = 'ACTIVO';
    }
    else {
        valueString = 'NO ACTIVO';
    }
    return valueString;
});

// var form = new app.CustomerInformationForm();
// form.render();


var createVoucherView = new app.CreateVoucherView({ el: $('#nuevo-cupon') });
var manageVoucherView = new app.ManageVoucherView({ el: $('#gestionar-cupones') });
var voucherResultsView = new app.VoucherResultsView({ el: $('#resultados-buscar-cupones') });
var voucherMovementResultsView = new app.VoucherMovementResultsView({ el: $('#gestionar-movimientos-cupon') });
