var app = app || {};

app.Customer = Backbone.Model.extend({
    defaults: {
        name: 'nombre',
        surname: 'apellido',
        email: 'email',
        phone: 'phone',
        address: 'direccion'
    }
});