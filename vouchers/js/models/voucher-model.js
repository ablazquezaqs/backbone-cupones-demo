var app = app || {};

app.Voucher = Backbone.Model.extend({
    defaults: {
        voucherName: 'nombre cupon',
        customerName: 'nombre cliente',
        barcode: '420393434',
        creationDate: 'email',
        expirationDate: 'phone',
        initialAmount: 100,
        currentAmount: 50,
        active: true
    }
});