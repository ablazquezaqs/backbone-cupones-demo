var app = app || {};

app.VoucherResultsView = Backbone.View.extend({
    initialize: function () {
        this.el.id || console.error('Initializing VoucherResultsView without el id');

        // Create even bus for child views with a Mixin
        this.eventBusObj = {};
        _.extend(this.eventBusObj, Backbone.Events);

        this.voucherCollection = new app.VoucherCollection();

        // Build child views.
        this.voucherListView = new app.VoucherListView({
            el: $('#voucher-list-placeholder'),
            collection: this.voucherCollection,
            eventBus: this.eventBusObj
        });
        this.voucherDetailView = new app.VoucherDetailView({
            el: $('#voucher-detail-placeholder'),
            eventBus: this.eventBusObj
        });

        this.voucherCollection.fetch();
        // Listen to events risen by the child views.
        this.listenTo(this.eventBusObj, 'voucher:selected', this.voucherSelectedEvent);
    },
    // DOM events and callbacks
    events: {
        'click .btn.activar-cupon': 'activateVoucher',
        'click .btn.desactivar-cupon': 'deactivateVoucher',
        'change .ordenar': 'sortVouchers',
        'change .filtrar': 'filterVouchers',
    },
    activateVoucher: function () {
        var selectedVoucherModel = this.voucherListView.getSelectedVoucher();
        if (selectedVoucherModel && !selectedVoucherModel.get('active')) {
            selectedVoucherModel.save({ active: true }, { patch: true });
        }
    },
    deactivateVoucher: function () {
        var selectedVoucherModel = this.voucherListView.getSelectedVoucher();
        if (selectedVoucherModel && selectedVoucherModel.get('active')) {
            selectedVoucherModel.save({ active: false }, { patch: true });
        }
    },
    sortVouchers: function (event) {
        var option = $(event.target).val();
        this.voucherListView.sortVouchers(option);
    },
    filterVouchers: function (event) {
        var option = $(event.target).val();
        this.voucherListView.filterVouchers(option);
    },
    voucherSelectedEvent: function (voucher) {
        if (voucher) {
            $('.intro').addClass('hidden');
            $('.info').removeClass('hidden');
        }
        else {
            $('.info').addClass('hidden');
            $('.intro').removeClass('hidden');
        }
    }
});