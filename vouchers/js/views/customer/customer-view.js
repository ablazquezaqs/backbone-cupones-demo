var app = app || {};

app.CustomerView = Backbone.View.extend({
    tagName: 'div',
    className: 'col-xs-12 col-xs-6 cargadatoscliente',
    template: Handlebars.compile($('#customer-template').html()),

    initialize: function (options) {
        options && options.eventBus || console.error('Initializing CustomerView without eventBus');

        this.eventBus = options.eventBus;
    },
    events: {
        'click button': 'editCustomerClicked',
        'dblclick #customer-container': 'customerClicked'
    },
    customerClicked: function () {
        this.eventBus.trigger('customer:selected', this.model);
    },
    editCustomerClicked: function () {
        alert('edit' + this.model.get('name'));
    },
    render: function () {
        //this.el is what we defined in tagName. use $el to get access to jQuery html() function
        this.$el.html(this.template(this.model.attributes));
        return this;
    }
});