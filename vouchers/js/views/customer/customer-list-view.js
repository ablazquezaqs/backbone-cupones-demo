var app = app || {};

/**
 * Vista de la lista de clientes. 
 * Iniciarlo con el elemento donde se renderizará y la instancia de la colección.
 * Escucha los eventos de cambios en la colección y se renderiza cuando ocurren.
 */
app.CustomerListView = Backbone.View.extend({
    initialize: function (options) {
        // Comprobar los elementos necesarios
        this.el.id || console.error('Initializing CustomerView without el id');
        this.collection || console.error('Initializing CustomerView without collection');
        options && options.eventBus || console.error('Initializing CustomerSearchView withou eventBus');

        // Asignar el bus de eventos
        this.eventBus = options.eventBus;

        // Vincularse a los eventos de la coleción.
        this.listenTo(this.collection, 'add', this.renderCustomer);
        // Escuchar los eventos lanzados por otras vistas.
        this.listenTo(this.eventBus, 'customer:search', this.searchCustomers);

        this.listenTo(this.collection, 'update', function () {
            this.updateDone = true;
        });
        this.updateDone = true;
        var self = this;
        $("#myModal .modal-body").on('scroll', function () {
            if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight && self.updateDone) {
                self.updateDone = false;
                $('#loading-indicator').show();
                self.collection.getNextPage().done(function () {
                    $('#loading-indicator').hide();
                });
            }
        });

    },
    searchCustomers: function (data) {
        console.log('Customer search with data:');
        console.log(data);
        data = {};
        var searchCriteria = { data: $.param(data) }

        this.collection.getFirstPage();
        this.show();
    },
    show: function () {
        // Vaciar el elemento html antes de mostrar el modal.
        this.$el.empty();
        $('#myModal').modal('show');
    },
    render: function () {
        // Para cada elemento de la colección renderizar individualmente.
        this.collection.each(function (customer) {
            this.renderCustomer(customer);
        }, this);
    },
    renderCustomer: function (customer) {
        // Crear un CustomerView para cada modelo de customer.
        var customerView = new app.CustomerView({
            model: customer,
            eventBus: this.eventBus
        });
        // Añadir el resultado al elemento de esta vista.
        this.$el.append(customerView.render().el);
    }
});