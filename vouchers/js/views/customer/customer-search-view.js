var app = app || {};

/**
 * Vista de formulario de busqueda de cliente.
 * Responde al evento click de un button que sea hijo del elemento con el que
 * se instancia.
 * La funcion que responde al evento de click recoge los valores del formulario
 * y eleva un evento con los datos recogidos.
 * @constructor
 * @extends Backbone.View
 * @see The <a href="http://backbonejs.org/#View">Backbone view doc</a>.
 */
app.CustomerSearchView = Backbone.View.extend({
    el: '#customer-information-form-placeholder',
    template: _.template($('#customer-information-form-template').html()),
    initialize: function (options) {
        options && options.eventBus || console.error('Initializing CustomerSearchView without eventBus');
        this.eventBus = options.eventBus;

        this.form = new app.CustomerInformationForm({
            el: $('#customer-information-form-placeholder'),
            template: this.template
        }).render();
        this.$el.append(this.form.el);
    },
    events: {
        'click .btn.search': 'searchCustomer'
    },
    searchCustomer: function (event) {
        event.preventDefault();
        var errors = this.form.validate();
        if (!errors) {
            var formData = this.form.getValue();
            this.eventBus.trigger('customer:search', formData);
        }
    },
    clearForm: function () {
        this.form.clear();
    }
});