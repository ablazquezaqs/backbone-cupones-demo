var app = app || {};

app.CustomerInformationForm = Backbone.Form.extend({
    el: '#customer-information-form-placeholder',
    template: _.template($('#customer-information-form-template').html()),
    schema: {
        name: {
            type: 'Text',
            // validators: ['required'],
            editorAttrs: { 'data-i18n': "[placeholder]name_and_surname", placeholder: 'Nombre y Apellidos' }, title: ''
        },
        dni: {
            type: 'Text',
            validators: [{ type: 'regexp', regexp: /\d{8}\D{1}/, message: $.i18n('valid_dni') }],
            editorAttrs: { 'data-i18n': "[placeholder]dni", placeholder: 'DNI' }, title: ''
        },
    },
    clear: function () {
        this.setValue({
            name: '',
            dni: ''
        });
    }
});