var app = app || {};

app.SelectedCustomerView = Backbone.View.extend({
    initialize: function (options) {
        this.el.id || console.error('Initializing SelectedCustomerView without el id');
        options && options.eventBus || console.error('Initializing CustomerView without eventBus');

        this.eventBus = options.eventBus;

        this.listenTo(this.eventBus, 'customer:selected', this.render);
    },
    getSelectedCustomer: function () {
        return this.model;
    },
    render: function (customer) {
        this.model = customer;
        var customerView = new app.CustomerView({
            model: customer
        });
        this.$el.html(customerView.render().el);
    }
});