var app = app || {};

/**
 * Vista de la lista de clientes. 
 * Iniciarlo con el elemento donde se renderizará y la instancia de la colección.
 * Escucha los eventos de cambios en la colección y se renderiza cuando ocurren.
 */
app.VoucherMovementListView = Backbone.View.extend( /** @lends BaseView.prototype */ {
    initialize: function (options) {
        // Comprobar los elementos necesarios
        this.el.id || console.error('Initializing VoucherMovementListView without el id');
        this.collection || console.error('Initializing VoucherMovementListView without collection');
        options && options.eventBus || console.error('Initializing VoucherMovementListView without eventBus');

        // Establecer el bus de eventos.
        this.eventBus = options.eventBus;

        this.listenTo(this.collection, 'update reset sort', this.render)
        this.listenTo(this.eventBus, 'voucher-movement:selected', this.voucherMovementSelected);
        this.selectedVoucherMovementView = null;
    },
    sortVoucherMovements: function (option) {
        switch (option) {
            case 'voucherName':
                this.filteredCollection.comparator = option;
                break;
            case 'barcode':
                this.filteredCollection.comparator = option;
                break;
            case 'importe-menor-mayor':
                this.filteredCollection.comparator = 'currentAmount';
                break;
            case 'importe-mayor-menor':
                this.filteredCollection.comparator = function (voucher) {
                    return -voucher.get('currentAmount');
                };
                break;
            case 'expirationDate':
                this.filteredCollection.comparator = option;
                break;
            case 'creationDate':
                this.filteredCollection.comparator = option;
                break;
            default:
                break;
        }
        this.filteredCollection.sort();
    },
    voucherMovementSelected: function (voucherMovementView) {
        if (this.selectedVoucherMovementView) {
            this.selectedVoucherMovementView.$el.toggleClass('active')
        }
        voucherMovementView.$el.toggleClass('active');
        this.selectedVoucherMovementView = voucherMovementView;
    },
    // getSelectedVoucher: function () {
    //     return this.selectedVoucherView.model;
    // },
    render: function () {
        // Vaciar el elemento html antes de renderizar la colección.
        this.$el.empty();
        // Para cada elemento de la colección renderizar individualmente.
        this.collection.each(function (voucherMovement) {
            this.renderVoucherMovement(voucherMovement);
        }, this);
    },
    renderVoucherMovement: function (voucherMovement) {
        // Crear un CustomerView para cada modelo de customer.
        var voucherMovementView = new app.VoucherMovementView({
            model: voucherMovement,
            eventBus: this.eventBus
        });
        // Añadir el resultado al elemento de esta vista.
        this.$el.append(voucherMovementView.render().el);
    }
});