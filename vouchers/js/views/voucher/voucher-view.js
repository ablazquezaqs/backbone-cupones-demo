var app = app || {};

app.VoucherView = Backbone.View.extend({
    tagName: 'li',
    className: 'cupon',
    template: Handlebars.compile($('#voucher-template').html()),

    initialize: function (options) {
        options && options.eventBus || console.error('Initializing VoucherView without eventBus');

        this.eventBus = options.eventBus;
        this.model.on('change:active', this.showActiveState, this);
    },
    events: {
        'click': 'voucherClicked'
    },
    voucherClicked: function () {
        console.log('voucher click');
        this.eventBus.trigger('voucher:selected', this);
    },
    showActiveState: function () {
        if (this.model.get('active') === false) {
            this.$el.addClass('deactivated');
        }
        else if (this.model.get('active') === true) {
            this.$el.removeClass('deactivated');
        }
    },
    render: function () {
        //this.el is what we defined in tagName. use $el to get access to jQuery html() function
        this.$el.html(this.template(this.model.attributes));
        this.showActiveState();
        return this;
    }
});