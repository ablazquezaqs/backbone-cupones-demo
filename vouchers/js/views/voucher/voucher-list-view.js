var app = app || {};

/**
 * Vista de la lista de clientes. 
 * Iniciarlo con el elemento donde se renderizará y la instancia de la colección.
 * Escucha los eventos de cambios en la colección y se renderiza cuando ocurren.
 */
app.VoucherListView = Backbone.View.extend( /** @lends BaseView.prototype */ {
    initialize: function (options) {
        // Comprobar los elementos necesarios
        this.el.id || console.error('Initializing VoucherListView without el id');
        this.collection || console.error('Initializing VoucherListView without collection');
        options && options.eventBus || console.error('Initializing VoucherListView withou eventBus');

        // Establecer el bus de eventos.
        this.eventBus = options.eventBus;

        this.filteredCollection = this.collection;
        this.listenTo(this.collection, 'update reset', function () {
            this.filteredCollection = this.collection;
            this.setCount();
            this.render();
        });
        this.listenTo(this.filteredCollection, 'sort', this.render);
        this.listenTo(this.eventBus, 'voucher:selected', this.voucherSelected);
        this.selectedVoucherView = null;
    },
    setCount: function () {
        $('.nresultados').html(this.filteredCollection.length + ' cupones');
    },
    sortVouchers: function (option) {
        switch (option) {
            case 'voucherName':
                this.filteredCollection.comparator = option;
                break;
            case 'barcode':
                this.filteredCollection.comparator = option;
                break;
            case 'importe-menor-mayor':
                this.filteredCollection.comparator = 'currentAmount';
                break;
            case 'importe-mayor-menor':
                this.filteredCollection.comparator = function (voucher) {
                    return -voucher.get('currentAmount');
                };
                break;
            case 'expirationDate':
                this.filteredCollection.comparator = option;
                break;
            case 'creationDate':
                this.filteredCollection.comparator = option;
                break;
            default:
                break;
        }
        this.filteredCollection.sort();
    },
    filterVouchers: function (option) {
        switch (option) {
            case 'todos':
                this.filteredCollection = this.collection;
                break;
            case 'activos':
                this.filteredCollection = this.collection.byActive(true);
                break;
            case 'desactivados':
                this.filteredCollection = this.collection.byActive(false);
                break;
            case 'caducados':
                this.filteredCollection = this.collection.byExpired();
                break;
            case 'consumidos':
                this.filteredCollection = this.collection.byConsumed();
                break;

            default:
                break;
        };
        this.listenTo(this.filteredCollection, 'sort', this.render);
        this.setCount();
        this.render();
    },
    voucherSelected: function (voucherView) {
        if (this.selectedVoucherView) {
            this.selectedVoucherView.$el.toggleClass('active')
        }
        voucherView.$el.toggleClass('active');
        this.selectedVoucherView = voucherView;
    },
    getSelectedVoucher: function () {
        return this.selectedVoucherView.model;
    },
    render: function () {
        // Vaciar el elemento html antes de renderizar la colección.
        this.$el.empty();
        // Para cada elemento de la colección renderizar individualmente.
        this.filteredCollection.each(function (voucher) {
            this.renderVoucher(voucher);
        }, this);
    },
    renderVoucher: function (voucher) {
        // Crear un CustomerView para cada modelo de customer.
        var voucherView = new app.VoucherView({
            model: voucher,
            eventBus: this.eventBus
        });
        // Añadir el resultado al elemento de esta vista.
        this.$el.append(voucherView.render().el);
    }
});