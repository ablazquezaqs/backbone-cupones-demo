var app = app || {};

app.VoucherDetailsView = Backbone.View.extend({
    initialize: function () {
        this.el.id || console.error('Initializing VoucherDetails without el id');
    },
    getVoucherDetails: function () {
        var formData = {};

        this.$el.find('input').each(function (i, childElem) {
            if ($(childElem).val() !== '') {
                formData[childElem.name] = $(childElem).val();
            }
        });
        // Si es vacio
        if (Object.getOwnPropertyNames(formData).length == 0) {
            formData = null;
        }
        return formData;
    },
    clearForm: function () {
        this.$el.find('input').each(function (i, childElem) {
            $(childElem).val('');
        });
    }
});