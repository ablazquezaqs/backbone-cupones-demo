var app = app || {};

/**
 * Vista de la lista de clientes. 
 * Iniciarlo con el elemento donde se renderizará y la instancia de la colección.
 * Escucha los eventos de cambios en la colección y se renderiza cuando ocurren.
 */
app.VoucherDetailView = Backbone.View.extend( /** @lends BaseView.prototype */ {
    template: Handlebars.compile($('#voucher-detail-template').html()),
    initialize: function (options) {
        // Comprobar los elementos necesarios
        this.el.id || console.error('Initializing VoucherDetailView without el id');
        options && options.eventBus || console.error('Initializing VoucherDetailView withou eventBus');

        this.eventBus = options.eventBus;

        this.listenTo(this.eventBus, 'voucher:selected', this.setVoucher);
    },
    setVoucher: function (voucher) {
        this.model = voucher.model;
        this.listenTo(this.model, 'change:active', this.render);
        this.render();
    },
    render: function (voucher) {
        this.$el.html(this.template(this.model.attributes));
    }
});