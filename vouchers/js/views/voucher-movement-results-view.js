var app = app || {};

app.VoucherMovementResultsView = Backbone.View.extend({
    initialize: function () {
        this.el.id || console.error('Initializing VoucherMovementResultsView without el id');

        var voucherMovementCollection = new app.VoucherMovementCollection();
        // // Create even bus for child views
        this.eventBusObj = {};
        // Mixin
        _.extend(this.eventBusObj, Backbone.Events);
        var voucherMovementListView = new app.VoucherMovementListView({
            el: $('#voucher-movement-list-placeholder'),
            collection: voucherMovementCollection,
            eventBus: this.eventBusObj
        });

        var voucherMovementDetailView = new app.VoucherMovementDetailView({
            el: $('#voucher-movement-detail-placeholder'),
            eventBus: this.eventBusObj
        });
        voucherMovementCollection.fetch();
        // Listen to events risen by the child views.
        this.listenTo(this.eventBusObj, 'voucher-movement:selected', this.voucherMovementSelectedEvent);
    },
    // DOM events and callbacks
    events: {
        'click .btn.activar-cupon': 'activateVoucher',
        'click .btn.desactivar-cupon': 'deactivateVoucher',
        'change .ordenar': 'sortVouchers',
        'change .filtrar': 'filterVouchers',
    },
    activateVoucher: function () {
        var selectedVoucherModel = this.voucherListView.getSelectedVoucher();
        if (selectedVoucherModel && !selectedVoucherModel.get('active')) {
            selectedVoucherModel.save({ active: true }, { patch: true });
        }
    },
    deactivateVoucher: function () {
        var selectedVoucherModel = this.voucherListView.getSelectedVoucher();
        if (selectedVoucherModel && selectedVoucherModel.get('active')) {
            selectedVoucherModel.save({ active: false }, { patch: true });
        }
    },
    sortVouchers: function (event) {
        var option = $(event.target).val();
        this.voucherListView.sortVouchers(option);
    },
    filterVouchers: function (event) {
        var option = $(event.target).val();
        this.voucherListView.filterVouchers(option);
    },
    voucherMovementSelectedEvent: function (voucherMovement) {
        if (voucherMovement) {
            $('.intro').addClass('hidden');
            $('.info').removeClass('hidden');
        }
        else {
            $('.info').addClass('hidden');
            $('.intro').removeClass('hidden');
        }
    }
});