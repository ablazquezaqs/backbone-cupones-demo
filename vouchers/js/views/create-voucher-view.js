var app = app || {};

app.CreateVoucherView = Backbone.View.extend({
    initialize: function () {
        this.el.id || console.error('Initializing CreateVoucherView without el id');

        // Create even bus for child views
        var eventBusObj = {};
        // Mixin
        _.extend(eventBusObj, Backbone.Events);

        // this.customerCollection = new app.CustomerCollection();
        this.customerCollection = new app.CustomerPageableCollection();


        this.customerSearchView = new app.CustomerSearchView({
            el: $('#customer-information-form-placeholder'),
            eventBus: eventBusObj
        });
        this.voucherDetailsView = new app.VoucherDetailsView({ el: $('#voucher-details-form') });
        this.customerListView = new app.CustomerListView({
            el: $('#customer-information-list-placeholder'),
            collection: this.customerCollection,
            eventBus: eventBusObj
        });
        this.selectedCustomerView = new app.SelectedCustomerView({
            el: $('#selected-customer-placeholder'),
            eventBus: eventBusObj
        });

        // Listen to events risen by the child views.
        // this.listenTo(eventBusObj, 'customer:search', this.searchCustomers);
    },
    // Callbacks for backbone events
    searchCustomers: function (data) {
        console.log('Customer search with data:');
        console.log(data);
        data = {};
        var searchCriteria = { data: $.param(data) }
        // this.customerCollection.fetch(searchCriteria);
        this.customerCollection.getFirstPage();
        $('#myModal').modal('show');
    },
    // DOM events and callbacks
    events: {
        'click .btn.add.crear-cupon': 'createVoucher',
        'click .btn.cancel': 'cleanForm'
    },
    createVoucher: function () {
        var formData = this.voucherDetailsView.getVoucherDetails();
        var selectedCustomer = this.selectedCustomerView.getSelectedCustomer();

        if (formData && selectedCustomer) {
            console.log('Creating voucher with:');
            console.log(formData);
            console.log(selectedCustomer.attributes);
            alert("Create voucher with data");
        }
        else {
            alert("Not enough data");
        }

    },
    cleanForm: function () {
        this.voucherDetailsView.clearForm();
        this.customerSearchView.clearForm();
    }
});