var app = app || {};

app.ManageVoucherView = Backbone.View.extend({
    initialize: function () {
        this.el.id || console.error('Initializing ManageVoucherView without el id');

        // Create even bus for child views
        var eventBusObj = {};
        // Mixin
        _.extend(eventBusObj, Backbone.Events);

        this.customerCollection = new app.CustomerCollection();
        this.customerSearchView = new app.CustomerSearchView({
            el: $('#search-by-customer-form'),
            eventBus: eventBusObj
        });
        this.voucherDetailsView = new app.VoucherDetailsView({ el: $('#voucher-details-manage-vouchers-form') });
        this.customerListView = new app.CustomerListView({
            el: $('#customer-information-list-placeholder'),
            collection: this.customerCollection,
            eventBus: eventBusObj
        });
        this.selectedCustomerView = new app.SelectedCustomerView({
            el: $('#selected-customer-manage-vouchers-placeholder'),
            eventBus: eventBusObj
        });

        // Listen to events risen by the child views.
        this.listenTo(eventBusObj, 'customer:search', this.searchCustomers);
    },
    // Callbacks for backbone events
    searchCustomers: function (data) {
        console.log('Customer search with data:');
        console.log(data);
        var searchCriteria = { data: $.param(data) }
        this.customerCollection.fetch(searchCriteria);
        $('#myModal').modal('show');
    },
    // DOM events and callbacks
    events: {
        'click .btn.search.btn-important': 'manageVoucher',
        'click .btn.cancel': 'cleanForm'
    },
    manageVoucher: function () {
        var formData = this.voucherDetailsView.getVoucherDetails();
        var selectedCustomer = this.selectedCustomerView.getSelectedCustomer();

        if (formData || selectedCustomer) {
            console.log('Managing voucher with:');
            console.log(formData);
            if (selectedCustomer) {
                console.log(selectedCustomer.attributes);
            }

            var searchCriteria = {
                formData: formData,
                selectedCustomer: selectedCustomer
            }
            // ocultar esta seccion
            this.$el.toggleClass('active in');
            Backbone.trigger('voucher:search', searchCriteria);
        }
        else {
            alert("Not enough data");
        }
    },
    cleanForm: function () {
        this.voucherDetailsView.clearForm();
        this.customerSearchView.clearForm();
    }

});