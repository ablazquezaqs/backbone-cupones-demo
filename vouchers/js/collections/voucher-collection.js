var app = app || {};

app.VoucherCollection = Backbone.Collection.extend({
    model: app.Voucher,
    url: 'http://localhost:3000/vouchers',
    // Filter collection
    // Activos, no activos, caducados, consumidos
    byActive: function (active) {
        filtered = this.filter(function (voucher) {
            return voucher.get("active") === active;
        });
        return new app.VoucherCollection(filtered);
    },
    byExpired: function () {
        var currentDate = new Date();
        filtered = this.filter(function (voucher) {
            return new Date(voucher.get("expirationDate")) < currentDate;
        });
        return new app.VoucherCollection(filtered);
    },
    byConsumed: function () {
        filtered = this.filter(function (voucher) {
            return voucher.get("currentAmount") <= 0;
        });
        return new app.VoucherCollection(filtered);
    }
});