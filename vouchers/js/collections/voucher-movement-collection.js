var app = app || {};

app.VoucherMovementCollection = Backbone.Collection.extend({
    model: app.VoucherMovement,
    url: 'http://localhost:3000/voucherMovements'
});