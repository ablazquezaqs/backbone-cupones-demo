var app = app || {};

app.CustomerCollection = Backbone.Collection.extend({
    model: app.Customer,
    url: 'http://localhost:3000/customers'
});